/*  --------------------------------------------------------------

PROBLEM 1

Check first whether the letter is a single character.
If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
If letter is invalid, return undefined.


E.G
"banana" -> 3

*/

    function countLetter(letter, sentence) 
    {
        let result = 0;
        if (letter.length === 1)
        {
                  
            
            for (let i = 0; i < sentence.length; i++) 
            {
               if (sentence[i].toLowerCase() == letter) 
                 {
                 result += 1;
                 }
             
            }
            return result; 
         }

        
        else
        {
            return undefined
        }
        
    }



/*  --------------------------------------------------------------

PROBLEM 2   

    An isogram is a word where there are no repeating letters.
    The function should disregard text casing before doing anything else.
    If the function finds a repeating letter, return false. Otherwise, return true.
    
    "cat" - isogram
    "pOol" - disregard casing (o and O are different in JS) 
        .toLowercase?
    */

    function isIsogram(text) 
    {
       let newArray = Array.from(text);
               
       let duplicateValues = []

        for (let i = 0; i < newArray.length; i++) 
        {
            let currentLetter = newArray[i]
            if (duplicateValues.indexOf(currentLetter) !== -1) 
            {
              return false
            }
            
            duplicateValues.push(currentLetter)
        }
        return true
                  
    }


/*  --------------------------------------------------------------

PROBLEM 3

    -Return undefined for people aged below 13.

    -Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)

    - Return the rounded off price for people aged 22 to 64. (NO DISCOUNT)
        always SHOW 2 DECIMALS (80.00)
    -The returned value should be a STRING.
    
    E.G
        item price: 100 
          AGE: 15 -> price: 80  
    
*/

    function purchase(age, price) 
    {

        if (age <= 13)
        {
            return undefined
        }
        else if (age > 13 &&  age < 21 || age > 64)
        {
            price = (price*.80).toFixed(2);
            return price.toString()
        }
        else
        {
            price = price.toFixed(2);
            return price.toString()
        }
        
    }


/*  --------------------------------------------------------------

PROBLEM 4

    Find categories that has no more stocks.
    The hot categories must be unique; no repeating categories.

    The passed items array from the test are the following:
    
    { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    The expected output after processing the items array is ['toiletries', 'gadgets'].
    Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

*/

    function findHotCategories(items) 
    {
        let hotCategories = []

        for (let i = 0; i < items.length; i++)
        {
            if (items[i].stocks === 0)
            {
                hotCategories.push(items[i].category)
            }
        }

        let uniqueCategory = [...new Set(hotCategories)];
        return uniqueCategory  

    }


/* --------------------------------------------------------------

PROBLEM 5

    Find voters who voted for both candidate A and candidate B.

    The passed values from the test are the following:
    candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
*/

    function findFlyingVoters(candidateA, candidateB) 
    {

        for (let i = 0; i < candidateA.length; i++)
        {
            const filteredArray = candidateA.filter(value => candidateB.includes(value));
            return filteredArray
        }
        
    }



/* EXPORT MODULE*/

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};